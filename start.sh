#!/bin/bash

#Good morning and let's start the day easy and with joy!
echo 'Be healthy, thankful and full of joy! Have a wonderful day!'

# ./netbeans-8.2/bin/netbeans &
atom &
php bin/console server:run &
firefox 'http://localhost:8000' &
firefox --new-window &

exit 0
