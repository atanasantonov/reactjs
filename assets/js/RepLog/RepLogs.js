import React from 'react';
import PropTypes from 'prop-types';
import RepLogList from './RepLogList';
import RepLogCreator from './RepLogCreator';
// import RepLogCreator from './RepLogCreatorControlledComponent';

function calculateTotaLifted(repLogs)  {

    let total = 0;

    for(let repLog of repLogs) {
        total += repLog.totalWeightLifted;
    }

    return total;

}

export default function RepLogs(props) {

    const {
        repLogs,
        itemOptions,
        withHeart,
        highlightedRowId,
        onRowClick,
        onAddRepLog,
        onDeleteRepLog,
        onHeartChange,
        numberOfHearts,
        isLoaded,
        isSavingNewRow,
        successMessage,
        newRepLogValidationErrorMessage,
    } = props;

    let heart = '';

    if(withHeart) {
        heart = <span>{':)'.repeat(numberOfHearts)}</span>;
    }

    return (<div>
        <h2>
            Lift History {heart}
        </h2>

        <input
            type="range"
            value={numberOfHearts}
            className="form-control"
            onChange={(e) => {
                onHeartChange(+e.target.value) // + converts a string value to number
            }}
        />

        {successMessage && (
            <div className="alert alert-success text-center">{successMessage}</div>
        )}

        <table className="table table-striped">
            <thead>
            <tr>
                <th>What</th>
                <th>How many times?</th>
                <th>Weight</th>
                <th>&nbsp;</th>
            </tr>
            </thead>

            <RepLogList
                itemOptions={itemOptions}
                repLogs={repLogs}
                highlightedRowId={highlightedRowId}
                onRowClick={onRowClick}
                onDeleteRepLog={onDeleteRepLog}
                isLoaded={isLoaded}
                isSavingNewRow={isSavingNewRow}
            />

            <tfoot>
                <tr>
                    <td>&nbsp;</td>
                    <th>Total</th>
                    <th>{calculateTotaLifted(repLogs)}</th>
                    <td>&nbsp;</td>
                </tr>
            </tfoot>
        </table>

        <div className="row">
            <div className="col-md-6">
                <RepLogCreator
                    itemOptions={itemOptions}
                    onAddRepLog={onAddRepLog}
                    validationErrorMessage={newRepLogValidationErrorMessage}
                />
            </div>
        </div>

    </div>);
}

RepLogs.propTypes = {
    repLogs: PropTypes.array.isRequired,
    itemOptions: PropTypes.array.isRequired,
    withHeart: PropTypes.bool,
    highlightedRowId: PropTypes.any,
    onRowClick: PropTypes.func.isRequired,
    onAddRepLog: PropTypes.func.isRequired,
    onDeleteRepLog: PropTypes.func.isRequired,
    onHeartChange: PropTypes.func.isRequired,
    numberOfHearts: PropTypes.number.isRequired,
    isLoaded: PropTypes.bool.isRequired,
    isSavingNewRow: PropTypes.bool.isRequired,
    successMessage: PropTypes.string.isRequired,
    newRepLogValidationErrorMessage: PropTypes.string.isRequired,
};
