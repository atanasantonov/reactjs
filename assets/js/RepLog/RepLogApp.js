import React, { Component } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid/v4';
import RepLogs from './RepLogs';
import RepLogList from './RepLogList';
import { getRepLogs, deleteRepLog, createRepLog } from '../api/rep_log_api';

export default class RepLogApp extends Component {

    constructor(props) {

        super(props);

        this.state = {
            highlightedRowId: null,
            repLogs: [],
            numberOfHearts: 1,
            isLoaded: false,
            isSavingNewRow: false,
            successMessage: '',
            newRepLogValidationErrorMessage: '',
        };

        this.successMessageTimeOutHandle = 0;

        this.handleRowClick = this.handleRowClick.bind(this);
        this.handleAddRepLog = this.handleAddRepLog.bind(this);
        this.handleDeleteRepLog = this.handleDeleteRepLog.bind(this);
        this.handleHeartChange = this.handleHeartChange.bind(this);
        this.setSuccessMessage = this.setSuccessMessage.bind(this);
    }

    componentDidMount() {
        getRepLogs()
            .then((data) => {
                this.setState({
                    repLogs: data,
                    isLoaded: true
                });
            });
    }

    componentWillUnmount() {
        clearTimeout(this.successMessageTimeOutHandle);
    }

    handleRowClick(repLogId) {
        this.setState({ highlightedRowId: repLogId });
    }

    handleAddRepLog(item, reps) {

        const newRep = {
            reps: reps,
            item: item
        }

        this.setState({
            isSavingNewRow: true
        });

        const newState = {
            isSavingNewRow: false,
        }

        createRepLog(newRep)
            .then(repLog => {
                this.setState(prevState => {
                    const newRepLogs = [...prevState.repLogs, repLog];

                    return {
                        ...newState,
                        repLogs: newRepLogs,
                        isSavingNewRow: false,
                    }

                });

                this.setSuccessMessage('Rep Log Saved!');
            })
            .catch(error => {
                error.response.json().then(errorsData => {
                    const errors = errorsData.errors;
                    const firstError = errors[Object.keys(errors)[0]];

                    this.setState({
                            newRepLogValidationErrorMessage: firstError,
                            ...newState,
                        });
                });
            })
    }

    setSuccessMessage(message) {
        this.setState({
            successMessage: message,
        });

        clearTimeout(this.successMessageTimeOutHandle);
        this.successMessageTimeOutHandle = setTimeout(()=>{
            this.setState({
                successMessage: ''
            });
            this.successMessageTimeOutHandle = 0;

        }, 2000);
    }

    handleDeleteRepLog(id) {

        this.setState((prevState) => {
            return {
                repLogs: prevState.repLogs.map(repLog => {
                    if(repLog.id !== id) {
                        return repLog;
                    }
                    return {...repLog, isDeleting: true};
                })
            }
        });

        deleteRepLog(id)
            .then(() => {
                this.setSuccessMessage('Rep Log Deleted!');

                this.setState((prevState) => {
                    const newRepLogs = prevState.repLogs.filter(repLog => repLog.id !== id);
                    return {repLogs: newRepLogs};
                });
            })
    }

    handleHeartChange(heartCount) {
        this.setState({
            numberOfHearts: heartCount
        });
    }

    render() {
        return (
            <RepLogs
                {...this.props}
                {...this.state}
                onRowClick = {this.handleRowClick}
                onAddRepLog = {this.handleAddRepLog}
                onDeleteRepLog = {this.handleDeleteRepLog}
                onHeartChange = {this.handleHeartChange}
            />
        );
    }
}

RepLogApp.propTypes = {
    withHeart: PropTypes.bool.isRequired,
    itemOptions: PropTypes.array.isRequired,
};

RepLogApp.defaultProps = {
    itemOptions: []
};
