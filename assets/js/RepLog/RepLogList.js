import React from 'react';
import PropTypes from 'prop-types';

export default function RepLogList(props) {

    const {
        repLogs,
        highlightedRowId,
        onRowClick,
        onDeleteRepLog ,
        isLoaded,
        isSavingNewRow,
    } = props;

    const handleDeleteClick = function(event, repLogId) {
        event.preventDefault();
        onDeleteRepLog(repLogId);
        return;
    };

    if(!isLoaded) {
        return (
            <tbody>
                <tr>
                    <td colSpan="4" className="text-center">Loading...</td>
                </tr>
            </tbody>
        );
    }

    return(
        <tbody>
        { repLogs.map((repLog) => (
            <tr
                key={repLog.id}
                className = { highlightedRowId === repLog.id ? 'info' : '' }
                onClick = { () => onRowClick(repLog.id) }
                style={{
                    opacity: repLog.isDeleting ? 0.3 : 1,
                }}
                >
                <td>{repLog.itemLabel}</td>
                <td>{repLog.reps}</td>
                <td>{repLog.totalWeightLifted}</td>
                <td>
                    <a href="#">
                        <i
                            className="fa fa-trash"
                            onClick = { (e) => handleDeleteClick(e, repLog.id) }
                        ></i>
                    </a>
                </td>
            </tr>
        ))}
        {isSavingNewRow && (
            <tr>
                <td
                    colSpan="4"
                    className="text-center"
                    style={{
                        opacity: .5
                    }}
                >Lifting to database...</td>
            </tr>
        )}
        </tbody>
    );

}

RepLogList.propTypes = {
    repLogs: PropTypes.array.isRequired,
    highlightedRowId: PropTypes.number,
    onRowClick: PropTypes.func,
    onDeleteRepLog: PropTypes.func.isRequired,
    isLoaded: PropTypes.bool.isRequired,
    isSavingNewRow: PropTypes.bool.isRequired,
};
